package com.supotato.uilaboratory.ui.view

import android.content.Context
import android.support.v4.view.NestedScrollingParent
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout

/**
 * Created by JonnyHsia on 17/9/13.
 */

class Test : LinearLayout, NestedScrollingParent {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}
}
