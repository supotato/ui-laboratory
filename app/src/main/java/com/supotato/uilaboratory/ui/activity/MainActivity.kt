package com.supotato.uilaboratory.ui.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import com.supotato.uilaboratory.R
import com.supotato.uilaboratory.adapter.LabAdapter
import com.supotato.uilaboratory.entity.Lab
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var labs: ArrayList<Lab> = ArrayList()
    private var mAdapter: LabAdapter? = null

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.supotato.uilaboratory.R.layout.activity_main)

        setUpRecyclerView()
    }

    private fun setUpRecyclerView() {
        prepareLabs()
        mRecyclerView.layoutManager = GridLayoutManager(this, 2)
        mRecyclerView.itemAnimator = DefaultItemAnimator()
        mRecyclerView.setHasFixedSize(true)
        mAdapter = LabAdapter(labs, { _, _, lab ->
            startActivity(Intent(this@MainActivity, lab.targetActivity))
        })
        mRecyclerView.adapter = mAdapter
    }

    private fun prepareLabs() {
        labs.clear()
        labs.add(Lab("Value Animation", ValueAnimActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Unlimited RecyclerView", LoopListActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Pager Snap Banner", PagerSnapActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("View Animation", ViewAnimationActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Bottom Sheet", BottomSheetActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Bottom Sheet Dialog", BottomSheetDialogActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Sticky Layout", StickyLayoutActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Multi Type", MultiTypeActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Emoji", EmojiActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Dialog", DialogActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Drawer", Main2Activity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("PowerView", PowerActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
    }
}