package com.supotato.uilaboratory.ui.multitype

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.supotato.uilaboratory.R
import com.supotato.uilaboratory.tools.findView

import me.drakeet.multitype.ItemViewBinder

/**
 * Created by JonnyHsia on 17/9/14.
 */
class HeaderViewBinder : ItemViewBinder<Header, HeaderViewBinder.ViewHolder>() {

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ViewHolder {
        val root = inflater.inflate(R.layout.item_header, parent, false)
        return ViewHolder(root)
    }

    override fun onBindViewHolder(holder: ViewHolder, header: Header) {
        holder.tvButton?.setOnClickListener {
            header.onClick(Unit)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvButton: TextView? = findView(R.id.tvButton)
    }
}
