package com.supotato.uilaboratory.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.supotato.uilaboratory.R
import com.supotato.uilaboratory.adapter.LabAdapter
import com.supotato.uilaboratory.entity.Lab
import com.supotato.uilaboratory.tools.toast
import kotlinx.android.synthetic.main.activity_sticky_layout.nestedScrollView

class StickyLayoutActivity : AppCompatActivity() {

    private var labs: ArrayList<Lab> = ArrayList()
    private var adapter: LabAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sticky_layout)

        prepareLabs()
        adapter = LabAdapter(labs, { view, pos, lab ->
            toast("$pos\n$lab")
        })
        nestedScrollView?.setHasFixedSize(true)
        nestedScrollView?.layoutManager = LinearLayoutManager(this)
        nestedScrollView?.adapter = adapter

        nestedScrollView?.post {
            toast("${nestedScrollView.measuredHeight}\n${nestedScrollView?.height}")
        }
    }

    private fun prepareLabs() {
        labs.clear()
        labs.add(Lab("Value Animation", ValueAnimActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Unlimited RecyclerView", LoopListActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Pager Snap Banner", PagerSnapActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("View Animation", ViewAnimationActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Bottom Sheet", BottomSheetActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Bottom Sheet Dialog", BottomSheetDialogActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Multi Type", StickyLayoutActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Dialog", DialogActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("Drawer", Main2Activity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
        labs.add(Lab("PowerView", PowerActivity::class.java, R.mipmap.ic_launcher, R.color.colorAccent))
    }
}
