package com.supotato.uilaboratory.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.view.View
import com.supotato.uilaboratory.R
import android.support.design.widget.BottomSheetBehavior
import kotlinx.android.synthetic.main.activity_bottom_sheet.bottomSheet
import android.util.DisplayMetrics
import android.util.Log
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.DecelerateInterpolator
import com.supotato.uilaboratory.tools.Utils
import com.supotato.uilaboratory.tools.toast
import com.supotato.uilaboratory.ui.SPBottomSheetCallback
import kotlinx.android.synthetic.main.activity_bottom_sheet.scrollView
import kotlinx.android.synthetic.main.activity_bottom_sheet.view


class BottomSheetActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_sheet)

        var scrollable = true
        val behavior = BottomSheetBehavior.from(bottomSheet)
/*
        behavior.setBottomSheetCallback(SPBottomSheetCallback({ _, slideOffset ->
            Log.d("BSB", "Offset: $slideOffset")
            scrollView.alpha = 1f - (slideOffset / 320)
        }, { _, newState ->
            toast("$newState")
        }))
*/
        behavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                scrollView.alpha = (1f - Math.pow((slideOffset * 0.75), 1.5)).toFloat()
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        // 恢复滑动与透明度
                        scrollable = true
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                        // 关闭滑动
                        scrollable = false
                    }
                }
            }
        })

        view.post {
            behavior.peekHeight = (view.height - Utils.dp2px(320f, this)).toInt()
        }

        scrollView.setOnTouchListener { _, _ ->
            return@setOnTouchListener !scrollable
        }
    }
}
