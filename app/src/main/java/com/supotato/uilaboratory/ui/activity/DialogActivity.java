package com.supotato.uilaboratory.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.supotato.uilaboratory.R;
import com.supotato.uilaboratory.ui.InputDialog;

public class DialogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        final TextView tvData = findViewById(R.id.tvData);
        Button button = findViewById(R.id.btnDialog);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputDialog dialog = new InputDialog();
                dialog.setOnSaveClick(new InputDialog.OnButtonClick() {
                    @Override
                    public void onSave(String data) {
                        tvData.setText(data);
                    }
                });
                dialog.show(getSupportFragmentManager(), "Number");
            }
        });
    }
}
