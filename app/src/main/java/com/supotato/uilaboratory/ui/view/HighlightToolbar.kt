package com.supotato.uilaboratory.ui.view

import android.content.Context
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CoordinatorLayout
import android.support.v4.view.NestedScrollingParent
import android.support.v4.view.ViewCompat
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.supotato.uilaboratory.R
import kotlin.properties.Delegates

/**
 * Created by JonnyHsia on 17/9/13.
 */
class HighlightToolbar : LinearLayout, NestedScrollingParent {

    constructor(context: Context) : super(context, null)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs, 0) {
        initView(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initView(context, attrs)
    }

    var tvTitle: TextView by Delegates.notNull()
    var tvSubTitle: TextView by Delegates.notNull()
    var imgIcon: ImageView by Delegates.notNull()

    private fun initView(context: Context, attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.view_hightlight_toolbar, this, true)
        tvTitle = findViewById(R.id.tvTitle)
        tvSubTitle = findViewById(R.id.tvSubTitle)
        imgIcon = findViewById(R.id.imgButton)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.HighlightToolbar)
        // 获取自定义控件的属性
        val title = typedArray.getString(R.styleable.HighlightToolbar_highlightTitle)
        val subTitle = typedArray.getString(R.styleable.HighlightToolbar_highlightSubTitle)
        val resource = typedArray.getResourceId(R.styleable.HighlightToolbar_icon, -1)
        typedArray.recycle()

        tvTitle.text = title
        tvSubTitle.text = subTitle
        imgIcon.setImageResource(resource)
    }
}