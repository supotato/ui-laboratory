package com.supotato.uilaboratory.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import com.supotato.uilaboratory.R
import com.supotato.uilaboratory.adapter.LabAdapter
import com.supotato.uilaboratory.entity.Lab
import com.supotato.uilaboratory.tools.toast
import com.supotato.uilaboratory.ui.multitype.Content
import com.supotato.uilaboratory.ui.multitype.ContentViewBinder
import com.supotato.uilaboratory.ui.multitype.Header
import com.supotato.uilaboratory.ui.multitype.HeaderViewBinder
import kotlinx.android.synthetic.main.activity_multi_type.multiTypeList
import me.drakeet.multitype.Items
import me.drakeet.multitype.MultiTypeAdapter

class MultiTypeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multi_type)

        initView()
    }

    var adapter: MultiTypeAdapter? = null
    var items: Items = Items()

    private fun initView() {
        adapter = MultiTypeAdapter(items)
        adapter?.register(Header::class.java, HeaderViewBinder())
        adapter?.register(Content::class.java, ContentViewBinder())

        val spanCount = 3
        val layoutManager = GridLayoutManager(this, spanCount)

        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val item = items[position]
                return if (item is Header) spanCount else 1
            }
        }
        multiTypeList?.setHasFixedSize(true)
        multiTypeList?.layoutManager = layoutManager
        multiTypeList?.adapter = adapter

        fakeData()
    }

    private fun fakeData() {
        Thread(Runnable {
            val newData = ArrayList<Content>()
            (0..20).mapTo(newData) { Content("标题$it") }
            items.add(Header({
                toast("Header")
            }))
            items.addAll(newData)
            runOnUiThread {
                adapter?.notifyDataSetChanged()
            }
        }).start()
    }
}
