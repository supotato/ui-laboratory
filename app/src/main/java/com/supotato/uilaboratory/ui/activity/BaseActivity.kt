package com.supotato.uilaboratory.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import com.supotato.uilaboratory.R

/**
 * Created by JonnyHsia on 17/6/21.
 * 基类 Activity
 */
abstract class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val toolbar: Toolbar? = findViewById(R.id.mToolbar)
        toolbar?.title = displayTitle()
        if (toolbar != null) {
            setSupportActionBar(toolbar)
        } else {
            Log.e("BaseActivity", "没有找到 Toolbar.")
        }
    }

    /**
     * 为活动设置标题
     */
    abstract fun displayTitle(): String
}