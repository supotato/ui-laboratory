package com.supotato.uilaboratory.ui.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.support.v4.graphics.ColorUtils
import android.util.AttributeSet
import android.view.View
import com.supotato.uilaboratory.R
import kotlin.properties.Delegates

/**
 * Created by JonnyHsia on 17/9/6.
 * 能力值
 */
class PowerView : View {

    /**
     * 能力名称
     */
    var powerName by Delegates.notNull<String>()

    /**
     * 能力指数
     */
    var power: Int = 0
        set(value) {
            field = value
            invalidate()
        }

    /**
     * 最大显示的指数
     */
    var maxPower = DEFAULT_MAX_POWER

    /**
     * 渐变或纯色
     */
    var powerShadingStyle = SHADING_GRADIENT

    /**
     * 纯颜色
     */
    var pureColor by Delegates.notNull<Int>()

    /**
     * 渐变起点色
     */
    var gradientStartColor by Delegates.notNull<Int>()

    /**
     * 渐变终点色
     */
    var gradientEndColor by Delegates.notNull<Int>()

    /**
     * 文字颜色
     */
    var textColor by Delegates.notNull<Int>()

    /**
     * 文字大小
     */
    var textSize by Delegates.notNull<Float>()

    /**
     * 绘制用
     */
    val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    /**
     * 文本范围
     */
    val bounds = Rect()

    /**
     * 控件初始化
     */
    private fun init(context: Context, attrs: AttributeSet?) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.PowerView)
        power = typedArray.getInteger(R.styleable.PowerView_power, 0)
        maxPower = typedArray.getInteger(R.styleable.PowerView_maxPower, DEFAULT_MAX_POWER)
        powerName = typedArray.getString(R.styleable.PowerView_power)
        powerShadingStyle = typedArray.getInteger(R.styleable.PowerView_powerShadingStyle, SHADING_GRADIENT)
        if (powerShadingStyle == SHADING_PURE) {
            pureColor = typedArray.getColor(R.styleable.PowerView_pureColor, Color.GRAY)
        } else {
            gradientStartColor = typedArray.getColor(R.styleable.PowerView_startColor, Color.GRAY)
            gradientEndColor = typedArray.getColor(R.styleable.PowerView_endColor, Color.GRAY)
        }
        textColor = typedArray.getColor(R.styleable.PowerView_textColor,
                context.resources.getColor(android.R.color.primary_text_light))
        textSize = typedArray.getDimension(R.styleable.PowerView_textSize, DEFAULT_TEXT_SIZE)
        typedArray.recycle()

        paint.color = textColor
        paint.textSize = textSize
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        paint.getTextBounds(powerName, 0, powerName.length, bounds)

        canvas?.drawText(powerName, 0f, 0f, paint)
        canvas?.drawText("$power", 0f, 0f, paint)
    }

    companion object {
        val SHADING_PURE = 0
        val SHADING_GRADIENT = 1
        val DEFAULT_MAX_POWER = 200
        val DEFAULT_TEXT_SIZE = 14f
    }

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }
}
