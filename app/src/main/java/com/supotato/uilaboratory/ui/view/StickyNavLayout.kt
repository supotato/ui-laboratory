package com.supotato.uilaboratory.ui.view

import android.animation.ValueAnimator
import android.content.Context
import android.support.v4.view.NestedScrollingChild2
import android.support.v4.view.NestedScrollingParent
import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.util.Log
import android.view.VelocityTracker
import android.view.View
import android.view.ViewConfiguration
import android.view.animation.Interpolator
import android.widget.LinearLayout
import android.widget.OverScroller

import com.supotato.uilaboratory.R

class StickyNavLayout(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs), NestedScrollingParent {

    override fun onStartNestedScroll(child: View, target: View, nestedScrollAxes: Int): Boolean {
        Log.e(TAG, "onStartNestedScroll")
        return true
    }

    override fun onNestedScrollAccepted(child: View, target: View, nestedScrollAxes: Int) {
        Log.e(TAG, "onNestedScrollAccepted")
    }

    override fun onStopNestedScroll(target: View) {
        Log.e(TAG, "onStopNestedScroll")
    }

    override fun onNestedScroll(target: View, dxConsumed: Int, dyConsumed: Int, dxUnconsumed: Int, dyUnconsumed: Int) {
        Log.e(TAG, "onNestedScroll")
    }

    override fun onNestedPreScroll(target: View, dx: Int, dy: Int, consumed: IntArray) {
        Log.e(TAG, "onNestedPreScroll")
        val hiddenTop = dy > 0 && scrollY < topViewHeight
        val showTop = dy < 0 && scrollY >= 0 && !ViewCompat.canScrollVertically(target, -1)

        if (hiddenTop || showTop) {
            scrollBy(0, dy)
            consumed[1] = dy
        }
    }

    private val TOP_CHILD_FLING_THRESHOLD = 3

    override fun onNestedFling(target: View, velocityX: Float, velocityY: Float, consumed: Boolean): Boolean {
        var isConsumed = consumed
        // 如果是recyclerView 根据判断第一个元素是哪个位置可以判断是否消耗
        // 这里判断如果第一个元素的位置是大于TOP_CHILD_FLING_THRESHOLD的
        // 认为已经被消耗，在animateScroll里不会对velocityY < 0时做处理
        if (target is RecyclerView && velocityY < 0) {
            val firstChild = target.getChildAt(0)
            val childAdapterPosition = target.getChildAdapterPosition(firstChild)
            isConsumed = childAdapterPosition > TOP_CHILD_FLING_THRESHOLD
        }
        if (!isConsumed) {
            animateScroll(velocityY, computeDuration(0f), consumed)
        } else {
            animateScroll(velocityY, computeDuration(velocityY), consumed)
        }
        return true
    }

    override fun onNestedPreFling(target: View, velocityX: Float, velocityY: Float): Boolean {
        //不做拦截 可以传递给子View
        return false
    }

    override fun getNestedScrollAxes(): Int {
        Log.e(TAG, "getNestedScrollAxes")
        return 0
    }

    /**
     * 根据速度计算滚动动画持续时间
     *
     * @param velocityY
     * @return
     */
    private fun computeDuration(vy: Float): Int {
        var velocityY = vy
        val distance: Int
        distance = if (velocityY > 0) {
            Math.abs(topView!!.height - scrollY)
        } else {
            Math.abs(topView!!.height - (topView!!.height - scrollY))
        }


        val duration: Int
        velocityY = Math.abs(velocityY)
        duration = if (velocityY > 0) {
            3 * Math.round(1000 * (distance / velocityY))
        } else {
            val distanceRatio = distance.toFloat() / height
            ((distanceRatio + 1) * 150).toInt()
        }

        return duration

    }

    private fun animateScroll(velocityY: Float, duration: Int, consumed: Boolean) {
        val currentOffset = scrollY
        val topHeight = topView!!.height
        if (offsetAnimator == null) {
            offsetAnimator = ValueAnimator()
            offsetAnimator!!.interpolator = interpolator
            offsetAnimator!!.addUpdateListener { animation ->
                if (animation.animatedValue is Int) {
                    scrollTo(0, animation.animatedValue as Int)
                }
            }
        } else {
            offsetAnimator!!.cancel()
        }
        offsetAnimator!!.duration = Math.min(duration, 600).toLong()

        if (velocityY >= 0) {
            offsetAnimator!!.setIntValues(currentOffset, topHeight)
            offsetAnimator!!.start()
        } else {
            // 如果子View没有消耗down事件 那么就让自身滑倒0位置
            if (!consumed) {
                offsetAnimator!!.setIntValues(currentOffset, 0)
                offsetAnimator!!.start()
            }

        }
    }

    private var topView: View? = null
    private var stickyView: View? = null
    private var nestedView: RecyclerView? = null

    private var topViewHeight: Int = 0

    private val scroller: OverScroller
    private var velocityTracker: VelocityTracker? = null
    private var offsetAnimator: ValueAnimator? = null
    private val interpolator: Interpolator? = null
    private val touchSlop: Int
    private val maximumVelocity: Int
    private val minimumVelocity: Int

    private val mLastY: Float = 0.toFloat()
    private val mDragging: Boolean = false

    init {
        orientation = LinearLayout.VERTICAL

        scroller = OverScroller(context)
        touchSlop = ViewConfiguration.get(context).scaledTouchSlop
        maximumVelocity = ViewConfiguration.get(context)
                .scaledMaximumFlingVelocity
        minimumVelocity = ViewConfiguration.get(context)
                .scaledMinimumFlingVelocity

    }

    private fun initVelocityTrackerIfNotExists() {
        if (velocityTracker == null) {
            velocityTracker = VelocityTracker.obtain()
        }
    }

    private fun recycleVelocityTracker() {
        if (velocityTracker != null) {
            velocityTracker!!.recycle()
            velocityTracker = null
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        topView = findViewById(R.id.topView)
        stickyView = findViewById(R.id.stickyView)
        val nestedScrollView = findViewById<View>(R.id.nestedScrollView) as? NestedScrollingChild2 ?: throw RuntimeException("XXX")
        nestedView = nestedScrollView as? RecyclerView
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        // 不限制顶部的高度
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        getChildAt(0).measure(widthMeasureSpec, View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
        val params = nestedView!!.layoutParams
        params.height = measuredHeight - stickyView!!.measuredHeight
        setMeasuredDimension(measuredWidth,
                topView!!.measuredHeight + stickyView!!.measuredHeight + nestedView!!.measuredHeight)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        topViewHeight = topView!!.measuredHeight
    }


    fun fling(velocityY: Int) {
        scroller.fling(0, scrollY, 0, velocityY, 0, 0, 0, topViewHeight)
        invalidate()
    }

    override fun scrollTo(x: Int, y: Int) {
        var newY = y
        if (newY < 0) {
            newY = 0
        }
        if (newY > topViewHeight) {
            newY = topViewHeight
        }
        if (newY != scrollY) {
            super.scrollTo(x, newY)
        }
    }

    override fun computeScroll() {
        if (scroller.computeScrollOffset()) {
            scrollTo(0, scroller.currY)
            invalidate()
        }
    }

    companion object {
        private val TAG = "StickyNavLayout"
    }

}