package com.supotato.uilaboratory.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.supotato.uilaboratory.R
import com.supotato.uilaboratory.ui.view.ValuePicker
import kotlinx.android.synthetic.main.activity_picker.*
import kotlinx.android.synthetic.main.app_bar.mToolbar

class PickerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)

        mToolbar.title = "Value Picker"
        setSupportActionBar(mToolbar)

        mBtnSimpleDatePicker.setOnClickListener {
            val picker = ValuePicker()
            picker.show(supportFragmentManager, "SimplePicker")
        }
    }
}
