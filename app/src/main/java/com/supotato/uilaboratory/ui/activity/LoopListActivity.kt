package com.supotato.uilaboratory.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import com.supotato.uilaboratory.R
import com.supotato.uilaboratory.adapter.LoopAdapter
import com.supotato.uilaboratory.tools.Utils
import com.supotato.uilaboratory.tools.toast
import kotlinx.android.synthetic.main.activity_unlimited_list.mRecyclerView

class LoopListActivity : AppCompatActivity() {

    private val DATA_SIZE = 30
    private var mData: Array<String?> = arrayOfNulls<String>(DATA_SIZE)
    private var mAdapter: LoopAdapter<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unlimited_list)

        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        prepareData()
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mRecyclerView.itemAnimator = DefaultItemAnimator()
        mAdapter = LoopAdapter(mData, { pos: Int, data: String ->
            mRecyclerView.smoothScrollToPosition(pos)
            /*toast(data)*/
        })
        mRecyclerView.adapter = mAdapter
        mRecyclerView.scrollToPosition(Utils.numberLower(Int.MAX_VALUE / 2, DATA_SIZE))
        LinearSnapHelper().attachToRecyclerView(mRecyclerView)
    }

    private fun prepareData() {
        if (DATA_SIZE < 1) {
            toast("Error")
            return
        }
        for (i in 0..DATA_SIZE - 1) {
            mData[i] = "$i"
        }
    }

}