package com.supotato.uilaboratory.ui.fragment

import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.app.Fragment
import android.view.View

import com.supotato.uilaboratory.R
import android.support.design.widget.BottomSheetBehavior
import android.widget.Button

/**
 * BottomSheetDialog
 */
class SPBottomSheetDialogFragment : BottomSheetDialogFragment() {

    private var behavior: BottomSheetBehavior<View>? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        // 创建 Dialog 的内容布局
        val view = View.inflate(context, R.layout.dialog_sheet, null)
        dialog.setContentView(view)
        // 设置 Dialog 的 UI Visibility 与点击监听
        view.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        view.findViewById<Button>(R.id.btnDialog).setOnClickListener { dismiss() }
        // 获取 Dialog 内容布局的 parent
        val root = view.parent as? View
        // 设置 root 的背景色为透明, 并获取其 behavior
        root?.setBackgroundColor(resources.getColor(android.R.color.transparent))
        behavior = BottomSheetBehavior.from(root)

        return dialog
    }

    override fun onStart() {
        super.onStart()
        // 展开 BottomSheet
        behavior?.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun dismiss() {
        // 收缩 BottomSheet
        behavior?.state = BottomSheetBehavior.STATE_HIDDEN
        super.dismiss()
    }
}
