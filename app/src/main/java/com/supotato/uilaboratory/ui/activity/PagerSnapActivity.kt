package com.supotato.uilaboratory.ui.activity

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.PagerSnapHelper
import android.util.Log
import android.view.View
import com.supotato.uilaboratory.GlideApp

import com.supotato.uilaboratory.R
import com.supotato.uilaboratory.adapter.AlbumAdapter
import com.supotato.uilaboratory.adapter.AppSectionAdapter
import com.supotato.uilaboratory.adapter.BannerAdapter
import com.supotato.uilaboratory.entity.App
import com.supotato.uilaboratory.entity.AppAlbum
import com.supotato.uilaboratory.entity.Banner
import com.supotato.uilaboratory.tools.toast
import kotlinx.android.synthetic.main.activity_located_banner.*

class PagerSnapActivity : BaseActivity() {

    private var mBanners: ArrayList<Banner> = ArrayList()
    private var mBannerAdapter: BannerAdapter? = null
    private var mApps: ArrayList<App> = ArrayList()
    private var mAppAdapter: AppSectionAdapter? = null
    private var mAlbum: ArrayList<AppAlbum> = ArrayList()
    private var mAlbumAdapter: AlbumAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_located_banner)

        GlideApp.with(this)
                .load(R.mipmap.avatar)
                .into(mImgAvatar)
        setupBanner()
        setUpCategory()
        setupSections()
        setupTitle()
    }

    private fun setupTitle() {
        // @drawable:underline
        mTvPageTitle.post {
            val width = mTvPageTitle.measuredWidth
            val divider = mTvPageTitle.compoundDrawablesRelative[3]
            Log.d("PagerDivider", "Left: ${divider.bounds.left}" +
                    "Top: ${divider.bounds.top}" +
                    "Bottom: ${divider.bounds.bottom}")
            // 只修改 Drawable 的宽度
            divider?.setBounds(divider.bounds.left, divider.bounds.top, width, divider.bounds.bottom)
        }
    }

    private fun setUpCategory() {
        prepareCategoryData()
        mAppAlbum.setHasFixedSize(true)
        mAppAlbum.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        mAppAlbum.itemAnimator = DefaultItemAnimator()
        PagerSnapHelper().attachToRecyclerView(mAppAlbum)
        mAlbumAdapter = AlbumAdapter(mAlbum, { _: View, _: Int, url: String ->
            toast(url)
        })
        mAppAlbum.adapter = mAlbumAdapter
    }

    private fun prepareCategoryData() {
        mAlbum.add(AppAlbum(R.mipmap.album_learn))
        mAlbum.add(AppAlbum(R.mipmap.album_puzzle))
        mAlbum.add(AppAlbum(R.mipmap.album_learn))
        mAlbum.add(AppAlbum(R.mipmap.album_puzzle))
    }

    private fun setupSections() {
        prepareSectionAppData()
        mSection.setHasFixedSize(true)
        mSection.itemAnimator = DefaultItemAnimator()
        mSection.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false)
        mAppAdapter = AppSectionAdapter(mApps, { _: View, pos: Int, app: App ->
            toast("Position: $pos\nApp: ${app.title}")
        })
        mSection.adapter = mAppAdapter

        LinearSnapHelper().attachToRecyclerView(mSection)
    }

    private fun prepareSectionAppData() {
        mApps.add(App("究极吃软饭的生活", "为了维持吃软饭的生活，我要许多零用钱才行！", R.mipmap.ic_ruanfan, true))
        mApps.add(App("猫咪的毛 ~", "摸摸猫，收集很多毛毛球制作各种小玩意儿吧！", R.mipmap.ic_caty, true))
        mApps.add(App("Astra", "探索阿斯特拉宇宙中的奇观和秘密。", R.mipmap.ic_astra))
        mApps.add(App("The Trial", "加入探险先锋队，体验荒野探险家的美妙经历。", R.mipmap.ic_cat))
        mApps.add(App("究极吃软饭的生活", "为了维持吃软饭的生活，我要许多零用钱才行！", R.mipmap.ic_ruanfan, true))
        mApps.add(App("猫咪的毛 ~", "摸摸猫，收集很多毛毛球制作各种小玩意儿吧！", R.mipmap.ic_caty, true))
    }

    private fun setupBanner() {
        prepareBannerData()
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.itemAnimator = DefaultItemAnimator()
        mRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        mBannerAdapter = BannerAdapter(mBanners, { _: View, _: Int, (title) ->
            toast("Title: $title")
        })
        mRecyclerView.adapter = mBannerAdapter
        val snapHelper = PagerSnapHelper()  // 竖屏下 PagerSnapHelper 效果更好
        snapHelper.attachToRecyclerView(mRecyclerView)
    }

    private fun prepareBannerData() {
        mBanners.add(Banner("跳躍吧! 鯉魚王!", "最弱寶可夢？以鯉魚王為主角的養成遊戲！", R.mipmap.magikarp, "全新游戏"))
        mBanners.add(Banner("Pokédex", "为玩家量身定做的宝可梦图鉴。", R.mipmap.pika, "编辑推荐"))
        mBanners.add(Banner("Zenge", "同 Eon 在世界和时间之间孤独流浪。", R.mipmap.zenge, "限时优惠"))
        mBanners.add(Banner("Google I/O 2017", "在移动设备上参与 Google I/O 大会。", R.mipmap.google, "全新应用"))
        mBanners.add(Banner("Jump! Magikarp!", "最弱寶可夢？以鯉魚王為主角的養成遊戲！", R.mipmap.magikarp, "Editors' Choices"))
    }

    override fun displayTitle(): String = "自动定位列表"
}
