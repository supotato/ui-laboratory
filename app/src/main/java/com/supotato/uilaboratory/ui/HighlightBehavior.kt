package com.supotato.uilaboratory.ui

import android.content.Context
import android.support.design.widget.CoordinatorLayout
import android.util.AttributeSet
import android.view.View
import com.supotato.uilaboratory.ui.view.HighlightToolbar

/**
 * Created by JonnyHsia on 17/9/13.
 */
class HighlightBehavior : CoordinatorLayout.Behavior<HighlightToolbar> {
    private val TAG = "ToolbarAlphaBehavior"
    private val offset = 0
    private val startOffset = 0
    private val endOffset = 0

    constructor() : super()

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    override fun onStartNestedScroll(coordinatorLayout: CoordinatorLayout,
                                     child: HighlightToolbar,
                                     directTargetChild: View,
                                     target: View,
                                     axes: Int,
                                     type: Int): Boolean {
        return true
    }

    override fun onNestedScroll(coordinatorLayout: CoordinatorLayout,
                                child: HighlightToolbar,
                                target: View,
                                dxConsumed: Int,
                                dyConsumed: Int,
                                dxUnconsumed: Int,
                                dyUnconsumed: Int,
                                type: Int) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, type)

    }
}