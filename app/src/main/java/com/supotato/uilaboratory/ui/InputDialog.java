package com.supotato.uilaboratory.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.supotato.uilaboratory.R;

import static android.text.InputType.TYPE_CLASS_NUMBER;
import static android.text.InputType.TYPE_NUMBER_FLAG_SIGNED;

/**
 * Created by JonnyHsia on 17/7/1.
 */

public class InputDialog extends DialogFragment {

    public interface OnButtonClick {
        void onSave(String data);
    }

    OnButtonClick mOnSaveClick;

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_input, null);
        final EditText input = view.findViewById(R.id.inputDialog);
        input.requestFocus();

        String tag = getTag();
        if (tag.equals("Text")) {

        } else if (tag.equals("Number")) {
            input.setInputType(TYPE_CLASS_NUMBER | TYPE_NUMBER_FLAG_SIGNED);
        }

        builder.setView(view)
                .setTitle("标题")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (mOnSaveClick != null) {
                            mOnSaveClick.onSave(input.getText().toString());
                        }
                    }
                })
                .setNegativeButton("取消", null);

        Toast.makeText(getActivity(), getTag(), Toast.LENGTH_SHORT).show();
        Dialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        return dialog;
    }

    public void setOnSaveClick(OnButtonClick onSaveClick) {
        mOnSaveClick = onSaveClick;
    }
}
