package com.supotato.uilaboratory.ui.view

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.supotato.uilaboratory.R

/**
 * Created by JonnyHsia on 17/6/20.
 * 值选择器
 */
class ValuePicker : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.dialog_value_picker, container, false)
    }
}