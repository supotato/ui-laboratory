package com.supotato.uilaboratory.ui.view

import android.content.Context
import android.support.annotation.AttrRes
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import com.supotato.uilaboratory.R

/**
 * Created by JonnyHsia on 17/6/20.
 * 自定义长宽比例 View
 */
class AspectRatioImageView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, @AttrRes defStyleAttr: Int = 0) : ImageView(context, attrs, defStyleAttr) {
    private val TAG = "AspectRatioView"
    private var mAspectRatio: Float = 0.toFloat()

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.AspectRatioImageView)
        mAspectRatio = typedArray.getFloat(R.styleable.AspectRatioImageView_imgAspectRatio, 1f)
        if (mAspectRatio == 0f) {
            mAspectRatio = 1f
            Log.e(TAG, "You must specify an aspect ratio when using AspectRatioView.")
            Log.d(TAG, "Aspect ratio has been reset to default value(1f).")
        }
        typedArray.recycle()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (mAspectRatio != 0f) {
            val width = MeasureSpec.getSize(widthMeasureSpec)
            val height = (width / mAspectRatio).toInt()
            Log.d(TAG, "Measured height: " + height)
            super.onMeasure(
                    MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY))
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        }
    }

    /**
     * 给 View 设置长宽比, 并刷新重绘
     * @param aspectRatio 长宽比
     */
    fun setAspectRatio(aspectRatio: Float) {
        mAspectRatio = aspectRatio
        invalidate() // 刷新 View
    }
}
