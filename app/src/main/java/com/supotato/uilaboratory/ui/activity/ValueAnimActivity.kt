package com.supotato.uilaboratory.ui.activity

import android.view.animation.AccelerateDecelerateInterpolator
import kotlinx.android.synthetic.main.activity_value_anim.*
import kotlinx.android.synthetic.main.app_bar.*

class ValueAnimActivity : android.support.v7.app.AppCompatActivity() {

    private val COUNTER_VALUE: Int = 1000
    private var is_playing_anim = false

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.supotato.uilaboratory.R.layout.activity_value_anim)

        mToolbar?.title = "Value Animation"
        if (mToolbar != null) {
            setSupportActionBar(mToolbar)
        }

        mTvCounter.text = COUNTER_VALUE.toString()
        mBtnStart.setOnClickListener {
            startCounterAnim()
        }
    }

    /**
     * 开始播放倒计时的值动画
     */
    private fun startCounterAnim() {
        if (is_playing_anim) {
            return
        }
        val currentValue = mTvCounter.text.toString().toInt()
        val targetValue = Math.abs(currentValue - COUNTER_VALUE)
        val anim = android.animation.ValueAnimator
                .ofInt(currentValue, targetValue)
                .setDuration(800)
        anim.addUpdateListener {
            mTvCounter.text = anim.animatedValue.toString()
        }
        anim.addListener(object : android.animation.Animator.AnimatorListener {
            override fun onAnimationStart(anim: android.animation.Animator?) {
                is_playing_anim = true
            }

            override fun onAnimationEnd(anim: android.animation.Animator?) {
                is_playing_anim = false
            }

            override fun onAnimationCancel(anim: android.animation.Animator?) {}
            override fun onAnimationRepeat(anim: android.animation.Animator?) {}
        })
        anim.interpolator = AccelerateDecelerateInterpolator()
        anim.start()
    }
}
