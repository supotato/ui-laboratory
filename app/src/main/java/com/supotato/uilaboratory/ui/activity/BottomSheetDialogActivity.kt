package com.supotato.uilaboratory.ui.activity

import android.app.Dialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.app.DialogFragment
import android.view.View
import android.widget.Button
import com.supotato.uilaboratory.R
import com.supotato.uilaboratory.ui.fragment.SPBottomSheetDialogFragment
import kotlinx.android.synthetic.main.activity_bottom_sheet_dialog.btnDialogSheet
import kotlinx.android.synthetic.main.activity_bottom_sheet_dialog.btnDialogSheetFragment

class BottomSheetDialogActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_sheet_dialog)

        btnDialogSheet.setOnClickListener {
            val dialog = BottomSheetDialog(this)
            val view = layoutInflater.inflate(R.layout.dialog_sheet, null)
            view.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
            view.findViewById<Button>(R.id.btnDialog).setOnClickListener {
                dialog.dismiss()
            }
            dialog.setContentView(view)
            dialog.show()
        }

        btnDialogSheetFragment.setOnClickListener {
            val dialog = SPBottomSheetDialogFragment()
            dialog.dialog?.findViewById<View>(android.support.design.R.id.design_bottom_sheet)
                    ?.setBackgroundColor(resources.getColor(android.R.color.transparent))
            dialog.show(supportFragmentManager, "BottomSheetDialog")
            // dialog.findViewById<View>(android.support.design.R.id.design_bottom_sheet)?.setBackgroundColor(resources.getColor(android.R.color.transparent))
        }
    }
}
