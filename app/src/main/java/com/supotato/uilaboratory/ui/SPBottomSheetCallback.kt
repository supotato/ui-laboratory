package com.supotato.uilaboratory.ui

import android.support.design.widget.BottomSheetBehavior
import android.view.View

/**
 * Created by JonnyHsia on 17/9/11.
 */
class SPBottomSheetCallback(var onSlide: ((bottomSheet: View, slideOffset: Float) -> Unit)? = null,
                            var onStateChanged: ((bottomSheet: View, newState: Int) -> Unit)? = null)
    : BottomSheetBehavior.BottomSheetCallback() {

    override fun onSlide(bottomSheet: View, slideOffset: Float) {
        onSlide(bottomSheet, slideOffset)
    }

    override fun onStateChanged(bottomSheet: View, newState: Int) {
        onStateChanged(bottomSheet, newState)
    }

}