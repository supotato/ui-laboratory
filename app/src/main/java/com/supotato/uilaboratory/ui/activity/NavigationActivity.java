package com.supotato.uilaboratory.ui.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.supotato.uilaboratory.R;

public class NavigationActivity extends AppCompatActivity {

    private Fragment mHomeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        // 底部导航栏, 并设置导航监听
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                // 根据选中的导航项判断切换到哪个子页面
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        // 将 mHomeFragment 显示在 id 为 fragment_container 的"容器"中
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_container, mHomeFragment)
                                .commit();
                        return true;
                }
                return false;
            }
        });

    }

}
