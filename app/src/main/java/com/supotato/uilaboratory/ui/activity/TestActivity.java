package com.supotato.uilaboratory.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.supotato.uilaboratory.ui.InputDialog;

/**
 * Created by JonnyHsia on 17/7/1.
 */

public class TestActivity extends AppCompatActivity {

    @ColorInt int a;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //

        final TextView t = new TextView(this);
        InputDialog dialog = new InputDialog();
        dialog.setOnSaveClick(new InputDialog.OnButtonClick() {
            @Override
            public void onSave(String data) {
                t.setText(data);
            }
        });
        dialog.show(getSupportFragmentManager(), "Input Dialog");

    }
}
