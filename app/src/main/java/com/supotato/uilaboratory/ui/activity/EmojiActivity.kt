package com.supotato.uilaboratory.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.supotato.uilaboratory.R
import kotlinx.android.synthetic.main.activity_emoji.btnEmoji
import kotlinx.android.synthetic.main.activity_emoji.inputEmoji
import kotlinx.android.synthetic.main.activity_emoji.tvEmoji

class EmojiActivity : AppCompatActivity() {


    // [U+1F469] (WOMAN) + [U+200D] (ZERO WIDTH JOINER) + [U+1F4BB] (PERSONAL COMPUTER)
    private val WOMAN_TECHNOLOGIST = "\uD83D\uDC69\u200D\uD83D\uDCBB"

    // [U+1F469] (WOMAN) + [U+200D] (ZERO WIDTH JOINER) + [U+1F3A4] (MICROPHONE)
    private val WOMAN_SINGER = "\uD83D\uDC69\u200D\uD83C\uDFA4"

    val EMOJI = WOMAN_TECHNOLOGIST + " " + WOMAN_SINGER

    val watcher = object : TextWatcher {
        override fun afterTextChanged(e: Editable?) {

        }

        override fun beforeTextChanged(c: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(c: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_emoji)

        inputEmoji?.addTextChangedListener(watcher)
        tvEmoji?.text = getString(R.string.emoji_text_view, EMOJI)

        btnEmoji?.text = "\uD83D\uDC6E\uD83C\uDFFB\u200D♀"
    }
}
