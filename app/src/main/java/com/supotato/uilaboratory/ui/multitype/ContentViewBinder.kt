package com.supotato.uilaboratory.ui.multitype

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.supotato.uilaboratory.R
import com.supotato.uilaboratory.tools.findView

import me.drakeet.multitype.ItemViewBinder

/**
 * Created by JonnyHsia on 17/9/14.
 */
class ContentViewBinder : ItemViewBinder<Content, ContentViewBinder.ViewHolder>() {

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ViewHolder {
        val root = inflater.inflate(R.layout.item_content, parent, false)
        return ViewHolder(root)
    }

    override fun onBindViewHolder(holder: ViewHolder, content: Content) {
        holder.tvTitle.text = content.title
        holder.itemView.setOnClickListener {

        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitle: TextView = findView(R.id.tvTitle)
    }
}
