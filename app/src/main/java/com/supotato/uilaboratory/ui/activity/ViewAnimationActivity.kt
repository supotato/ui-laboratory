package com.supotato.uilaboratory.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AnimationUtils
import android.widget.NumberPicker

import com.supotato.uilaboratory.R
import kotlinx.android.synthetic.main.activity_view_animation.flipper

class ViewAnimationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_animation)

        flipper.inAnimation = AnimationUtils.loadAnimation(this, R.anim.push_up_in)
        flipper.outAnimation = AnimationUtils.loadAnimation(this, R.anim.push_up_out)

        flipper.startFlipping()
    }
}
