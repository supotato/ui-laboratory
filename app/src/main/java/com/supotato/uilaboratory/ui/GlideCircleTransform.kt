/*
package com.supotato.uilaboratory.ui

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.support.v4.graphics.drawable.RoundedBitmapDrawable
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation
import com.supotato.uilaboratory.tools.Utils
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.graphics.PixelFormat


*/
/**
 * Created by JonnyHsia on 17/6/21.
 *//*

class GlideCircleTransform(var context: Context) : BitmapTransformation(context) {

    override fun transform(pool: BitmapPool?, toTransform: Bitmap?, outWidth: Int, outHeight: Int): Bitmap {
        val drawable = RoundedBitmapDrawableFactory.create(context.resources, toTransform)
        drawable.isCircular = true
        val bitmap = drawableToBitmap(drawable)
        return bitmap
    }

    private fun drawableToBitmap(drawable: RoundedBitmapDrawable): Bitmap {

        // 取 drawable 的长宽
        val w = drawable.intrinsicWidth
        val h = drawable.intrinsicHeight

        // 取 drawable 的颜色格式
        val config = if (drawable.opacity != PixelFormat.OPAQUE)
            Bitmap.Config.ARGB_8888
        else
            Bitmap.Config.RGB_565
        // 建立对应 bitmap
        val bitmap = Bitmap.createBitmap(w, h, config)
        // 建立对应 bitmap 的画布
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, w, h)
        // 把 drawable 内容画到画布中
        drawable.draw(canvas)
        return bitmap
    }

    override fun getId(): String {
        return javaClass.name
    }
}*/
