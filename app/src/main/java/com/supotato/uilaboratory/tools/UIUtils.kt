package com.supotato.uilaboratory.tools

/**
 * Created by JonnyHsia on 17/9/14.
 */

object UIUtils {

    fun cleanString(dirtyString: String): String {
        return dirtyString.replace("((\r\n)|\n)[\\s\t ]*(\\1)+".toRegex(), "$1");
    }
}
