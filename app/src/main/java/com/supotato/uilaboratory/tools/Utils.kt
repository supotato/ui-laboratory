package com.supotato.uilaboratory.tools

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.Drawable
import android.support.v4.graphics.drawable.DrawableCompat
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.WindowManager
import android.content.res.TypedArray


/**
 * Created by JonnyHsia on 17/5/27.
 */
object Utils {

    /**
     * dp 转换为 px
     */
    fun dp2px(dp: Float, context: Context): Float {
        return dp2px(dp, context.resources.displayMetrics)
    }

    private fun dp2px(dp: Float, metrics: DisplayMetrics): Float {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics)
    }

    fun px2sp(context: Context, pxValue: Float): Float {
        return pxValue / (context.resources.displayMetrics.scaledDensity)
    }

/*    fun dp2px(context: Context, dpValue: Int): Int {
        val scale = context.resources.displayMetrics.density
        return (dpValue * scale + 0.5f).toInt()
    }

    fun px2dp(context: Context, pxValue: Int): Int {
        val scale = context.resources.displayMetrics.density
        return (pxValue / scale + 0.5f).toInt()
    }*/

    fun tintDrawable(drawable: Drawable, colors: Int): Drawable {
        val wrappedDrawable = DrawableCompat.wrap(drawable).mutate()
        DrawableCompat.setTint(wrappedDrawable, colors)
        return wrappedDrawable
    }

    /**
     * 获取屏幕宽度
     */
    fun getScreenWidth(context: Context): Int {
        val point = Point()
        (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager)
                .defaultDisplay
                .getSize(point)
        return point.x
    }

    /**
     * 打印 HTTP 错误
     */
    /*fun logHttpError(e: Throwable, tag: String = "RxHttpError") {
        if (e is HttpException) {
            val body = e.response().errorBody()
            Log.e(tag, body.string())
        }
    }*/

    /**
     * 颜色变浅处理
     * @param RGBValues
     */
    fun colorEasy(RGBValues: Int): Int {
        var red = RGBValues shr 16 and 0xff
        var green = RGBValues shr 8 and 0xff
        var blue = RGBValues and 0xff
        if (red == 0) {
            red = 10
        }
        if (green == 0) {
            green = 10
        }
        if (blue == 0) {
            blue = 10
        }
        red = Math.floor(red * (1 + 0.1)).toInt()
        green = Math.floor(green * (1 + 0.1)).toInt()
        blue = Math.floor(blue * (1 + 0.1)).toInt()
        return Color.rgb(red, green, blue)
    }

    /**
     * 颜色加深处理
     * @param colorValue
     */
    fun colorBurn(colorValue: Int?, defaultColor: Int = Color.parseColor("#ED000000")): Int {
        if (colorValue == null) {
            return defaultColor
        }
        var red = colorValue shr 16 and 0xff
        var green = colorValue shr 8 and 0xff
        var blue = colorValue and 0xff
        red = Math.floor(red * (1 - 0.1)).toInt()
        green = Math.floor(green * (1 - 0.1)).toInt()
        blue = Math.floor(blue * (1 - 0.1)).toInt()
        return Color.rgb(red, green, blue)
    }

    fun numberLower(number: Int, mod: Int): Int {
        val n = number - (number % mod)
        return n
    }

    fun contentSize(activity: Activity, haveStatusBar: Boolean = false): Int {
        val resources = activity.resources
        var statusBarHeight = 0
        val statusResourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (statusResourceId > 0) {
            statusBarHeight = resources.getDimensionPixelSize(statusResourceId)
        }

        // action bar height
        var actionBarHeight = 0
        val styledAttributes = activity.theme.obtainStyledAttributes(
                intArrayOf(android.R.attr.actionBarSize)
        )
        actionBarHeight = styledAttributes.getDimension(0, 0f).toInt()
        styledAttributes.recycle()

/*
        // navigation bar height
        var navigationBarHeight = 0
        val navResourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
        if (navResourceId > 0) {
            navigationBarHeight = resources.getDimensionPixelSize(navResourceId)
        }
*/

        val dm = DisplayMetrics()
        activity.windowManager.defaultDisplay.getRealMetrics(dm)

        return dm.heightPixels - actionBarHeight -
                if (haveStatusBar) statusBarHeight else 0
    }
}