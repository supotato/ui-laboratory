package com.supotato.uilaboratory.tools

import android.app.Activity
import android.graphics.Color
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import java.text.DecimalFormat

/**
 * Created by JonnyHsia on 17/6/20.
 * 插件方法
 */

/*fun Activity.findView(id: Int): View? {
    return this.findViewById(id)
}*/

fun Activity.toast(message: String, duration: Int = Toast.LENGTH_SHORT): Unit {
    Toast.makeText(this, message, duration).show()
}

fun Fragment.toast(message: String, duration: Int = Toast.LENGTH_SHORT): Unit {
    if (context != null) Toast.makeText(context, message, duration).show()
}

fun <T : View> RecyclerView.ViewHolder.findView(id: Int): T {
    return itemView.findViewById<T>(id)
}

fun Number.format(maxDigits: Int = 2): String {
    val formater = DecimalFormat()
    formater.maximumFractionDigits = maxDigits
    return formater.format(this)
}