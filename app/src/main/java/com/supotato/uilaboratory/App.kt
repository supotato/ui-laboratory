package com.supotato.uilaboratory

import android.app.Application
import android.support.text.emoji.EmojiCompat
import android.support.text.emoji.FontRequestEmojiCompatConfig
import android.support.text.emoji.bundled.BundledEmojiCompatConfig
import android.support.v4.provider.FontRequest
import android.util.Log

/**
 * Created by JonnyHsia on 17/9/15.
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        val BUNDLED = true
        val config: EmojiCompat.Config

        if (BUNDLED) {
            config = BundledEmojiCompatConfig(applicationContext)
        } else {
            val fontRequest = FontRequest("com.google.android.gms.fonts",
                    "com.google.android.gms",
                    "Noto Color Emoji Compat",
                    R.array.com_google_android_gms_fonts_certs)
            config = FontRequestEmojiCompatConfig(applicationContext, fontRequest)
                    .setReplaceAll(true)
                    .registerInitCallback(object : EmojiCompat.InitCallback() {
                        override fun onInitialized() {
                            super.onInitialized()
                            Log.i(TAG, "Emoji Compat initialized");
                        }

                        override fun onFailed(throwable: Throwable?) {
                            super.onFailed(throwable)
                            Log.e(TAG, "Emoji Compat initialization failed", throwable);
                        }
                    })
        }

        EmojiCompat.init(config)
    }

    companion object {
        val TAG = "App"
    }

}