package com.supotato.uilaboratory.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.supotato.uilaboratory.R
import com.supotato.uilaboratory.tools.findView
import com.supotato.uilaboratory.entity.AppAlbum

/**
 * Created by JonnyHsia on 17/6/21.
 *
 */
class AlbumAdapter(var mAlbums: ArrayList<AppAlbum>, val itemClick: (View, Int, String) -> Unit)
    : RecyclerView.Adapter<AlbumAdapter.VH>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): VH {
        val view = LayoutInflater.from(parent?.context)
                .inflate(R.layout.item_app_album, parent, false)
        return VH(view)
    }

    override fun onBindViewHolder(holder: VH?, position: Int) {
        val appAlbum = mAlbums[position]
        holder?.itemView?.setOnClickListener {
            itemClick.invoke(holder.itemView, position, appAlbum.url)
        }
        holder?.bind(appAlbum)
    }

    override fun getItemCount(): Int {
        return mAlbums.size
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mImgBanner: ImageView? = findView(R.id.imgBanner)

        fun bind(album: AppAlbum) {
            Glide.with(itemView.context)
                    .load(album.res)
                    .into(mImgBanner)
        }
    }
}