package com.supotato.uilaboratory.adapter

import android.graphics.BitmapFactory
import android.support.v7.graphics.Palette
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.supotato.uilaboratory.R
import com.supotato.uilaboratory.entity.Banner
import com.supotato.uilaboratory.tools.findView
import android.util.Log
import com.bumptech.glide.Priority
import com.supotato.uilaboratory.GlideApp
import com.supotato.uilaboratory.tools.Utils


/**
 * Created by JonnyHsia on 17/6/21.
 */
class BannerAdapter(var mBanners: ArrayList<Banner>, val itemClick: (View, Int, Banner) -> Unit)
    : RecyclerView.Adapter<BannerAdapter.VH>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): VH {
        val view = LayoutInflater.from(parent?.context)
                .inflate(R.layout.item_banner, parent, false)
        return VH(view)
    }

    override fun onBindViewHolder(holder: VH?, position: Int) {
        val banner = mBanners[position]
        holder?.itemView?.setOnClickListener {
            itemClick.invoke(holder.itemView, position, banner)
        }

        holder?.bind(banner)
    }

    override fun getItemCount(): Int {
        return mBanners.size
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView), Palette.PaletteAsyncListener {

        val mTvTitle: TextView? = findView(R.id.tvTitle)
        val mTvDescription: TextView? = findView(R.id.tvDescription)
        val mTvCategory: TextView? = findView(R.id.tvCategory)
        val mImgBanner: ImageView? = findView(R.id.imgBanner)

        fun bind(banner: Banner) {
            mTvTitle?.text = banner.title
            mTvDescription?.text = banner.description
            mTvCategory?.text = banner.category

            GlideApp.with(itemView)
                    .load(banner.res)
                    .centerCrop()
                    .placeholder(android.R.color.darker_gray)
                    .priority(Priority.HIGH)
                    .into(mImgBanner)

            /*Glide.with(itemView.context)
                    .load(banner.res)
                    .placeholder(android.R.color.darker_gray)
                    .bitmapTransform(GlideRoundTransform(itemView.context, 4))
                    .into(mImgBanner)*/

            Palette.Builder(BitmapFactory.decodeResource(itemView.resources, banner.res))
                    .generate({ palette ->
                        Log.d("Palette", "${palette.vibrantSwatch?.rgb}")
                        mTvCategory?.setTextColor(
                                Utils.colorBurn(palette.vibrantSwatch?.rgb))
                    })
        }


        override fun onGenerated(palette: Palette?) {
        }
    }
}

/*
private fun drawable2Bitmap(drawable: Drawable): Bitmap {
            val bitmap = Bitmap.createBitmap(
                    drawable.intrinsicWidth,
                    drawable.intrinsicHeight,
                    if (drawable.opacity != PixelFormat.OPAQUE)
                        Bitmap.Config.ARGB_8888
                    else
                        Bitmap.Config.RGB_565)

            val canvas = Canvas(bitmap)
            drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
            drawable.draw(canvas)
            return bitmap
        }
*/