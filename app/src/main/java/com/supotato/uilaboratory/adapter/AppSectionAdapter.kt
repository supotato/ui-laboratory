package com.supotato.uilaboratory.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView
import com.supotato.uilaboratory.R
import com.supotato.uilaboratory.entity.App
import com.supotato.uilaboratory.tools.findView
import com.supotato.uilaboratory.tools.format
import java.text.NumberFormat

/**
 * Created by JonnyHsia on 17/6/22.
 */
class AppSectionAdapter(var mApps: ArrayList<App>, val clickItem: (View, Int, App) -> Unit)
    : RecyclerView.Adapter<AppSectionAdapter.VH>() {

    override fun onBindViewHolder(holder: VH?, position: Int) {
        val app = mApps[position]

        holder?.bind(app)
        holder?.mBtnGet?.setOnClickListener {
            clickItem.invoke(holder.itemView, position, app)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): VH {
        val view = LayoutInflater.from(parent?.context)
                .inflate(R.layout.item_section_app, parent, false)
        return VH(view)
    }

    override fun getItemCount(): Int {
        return mApps.size
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mImgIcon: RoundedImageView? = findView(R.id.imgIcon)
        val mTvTitle: TextView? = findView(R.id.tvTitle)
        val mTvDescription: TextView? = findView(R.id.tvDescription)
        val mBtnGet: TextView? = findView(R.id.btnGet)
        val mTvInAppPurchases: TextView? = findView(R.id.tvInAppPurchases)

        fun bind(app: App): Unit {
            mTvTitle?.text = app.title
            mTvDescription?.text = app.description
            mTvInAppPurchases?.visibility = if (app.inAppPurchases) View.VISIBLE else View.GONE
            mBtnGet?.text = if (app.purchased && app.downloaded) "OPEN"
            else if (app.price > 0f) "$${app.price.format(2)}"
            else "GET"

            Glide.with(itemView.context)
                    .load(app.res)
                    .into(mImgIcon)
        }
    }
}