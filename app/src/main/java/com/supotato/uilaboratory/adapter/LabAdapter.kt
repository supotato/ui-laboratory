package com.supotato.uilaboratory.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.supotato.uilaboratory.R
import com.supotato.uilaboratory.entity.Lab
import com.supotato.uilaboratory.tools.findView

/**
 * Created by JonnyHsia on 17/6/20.
 * Lab 列表适配器
 */
class LabAdapter(var mLabs: ArrayList<Lab>, val itemClick: (View, Int, Lab) -> Unit)
    : RecyclerView.Adapter<LabAdapter.VH>() {
    private val TAG = "LabAdapter"

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): VH {
        val view = LayoutInflater.from(parent?.context)
                .inflate(R.layout.item_lab, parent, false)
        return VH(view)
    }

    override fun onBindViewHolder(holder: VH?, position: Int) {
        val lab = mLabs[position]

        /*holder?.itemView?.setBackgroundResource(lab.colorRes)*/
        holder?.mTvTitle?.text = lab.name
        holder?.mImgIcon?.setImageResource(lab.iconRes)
        holder?.itemView?.setOnClickListener {
            itemClick.invoke(holder.itemView, position, lab)
        }
    }

    override fun getItemCount(): Int {
        return mLabs.size
    }

    fun update(newData: ArrayList<Lab>) {
        mLabs = newData
        notifyDataSetChanged()
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mImgIcon: ImageView? = findView(R.id.imgIcon)
        val mTvTitle: TextView? = findView(R.id.tvTitle)
    }
}