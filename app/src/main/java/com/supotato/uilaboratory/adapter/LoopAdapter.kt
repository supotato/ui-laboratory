package com.supotato.uilaboratory.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.supotato.uilaboratory.tools.findView

/**
 * Created by JonnyHsia on 17/6/23.
 * 循环列表适配器
 */
class LoopAdapter<T>(var mData: Array<T?>, var itemClick: (Int, T) -> Unit)
    : RecyclerView.Adapter<LoopAdapter.VH>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): VH {
        val view = LayoutInflater.from(parent?.context)
                .inflate(android.R.layout.simple_list_item_1, parent, false)
        return VH(view)
    }

    override fun onBindViewHolder(holder: VH?, position: Int) {
        val data = mData[position % mData.size] ?: return

        if (data is String || data is Number) {
            holder?.mTextView?.text = "$data"
            holder?.itemView?.setOnClickListener {
                itemClick.invoke(position, data)
            }
        }
    }

    override fun getItemCount(): Int {
        if (mData.isNotEmpty()) {
            return Int.MAX_VALUE
        } else {
            return 0
        }
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mTextView: TextView? = findView(android.R.id.text1)
    }
}