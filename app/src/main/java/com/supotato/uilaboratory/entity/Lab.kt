package com.supotato.uilaboratory.entity

import android.app.Activity

/**
 * Created by JonnyHsia on 17/6/20.
 * Lab 实验室 POJO
 */
data class Lab(var name: String, var targetActivity: Class<*>, var iconRes: Int, var colorRes: Int)