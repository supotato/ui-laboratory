package com.supotato.uilaboratory.entity

/**
 * Created by JonnyHsia on 17/6/21.
 */
data class Banner(var title: String, var description: String, var res: Int, var category: String)