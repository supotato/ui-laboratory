package com.supotato.uilaboratory.entity

import java.io.FileDescriptor

/**
 * Created by JonnyHsia on 17/6/22.
 * App POJO
 */
data class App(var title: String,
               var description: String,
               var res: Int,
               var inAppPurchases: Boolean = false, // 内购
               var purchased: Boolean = false,      // 已购买
               var downloaded: Boolean = false,     // 已下载
               var price: Float = 0f                // 价格
)